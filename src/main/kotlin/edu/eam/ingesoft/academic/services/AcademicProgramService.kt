package edu.eam.ingesoft.academic.services

import edu.eam.ingesoft.academic.exceptions.BusinessException
import edu.eam.ingesoft.academic.model.entities.AcademicProgram
import edu.eam.ingesoft.academic.repositories.AcademicProgramRepository
import edu.eam.ingesoft.academic.repositories.FacultyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException

@Service
class AcademicProgramService {

    @Autowired
    lateinit var academicProgramRepository: AcademicProgramRepository

    @Autowired
    lateinit var facultyRepository: FacultyRepository

    fun create(academicProgram: AcademicProgram) {

        val existe = facultyRepository.existsById(academicProgram.faculty.id)
        if (!existe) {
            throw BusinessException("The Faculty does not exist!", HttpStatus.PRECONDITION_FAILED)
        }

        if (academicProgramRepository.existsById(academicProgram.id)) {
            throw BusinessException(
                "This program is already registered in the system: ",
                HttpStatus.PRECONDITION_FAILED
            )
        }

        if (!facultyRepository.existsById(academicProgram.faculty.id)) {
            throw BusinessException("The Faculty does not exist!", HttpStatus.PRECONDITION_FAILED)
        }

        if (academicProgramRepository.existsByName(academicProgram.name)) {
            throw BusinessException("This name is already regist in the system", HttpStatus.PRECONDITION_FAILED)
        }
        academicProgramRepository.save(academicProgram)
    }

    fun find(idProgram: Long): AcademicProgram {

        if (!academicProgramRepository.existsById(idProgram)) throw BusinessException(
            "The program does not exist",
            HttpStatus.NOT_FOUND
        )

        val academicProgram = academicProgramRepository.findById(idProgram)
        return academicProgram.get()
    }

    fun getProgramList(idFaculty: Long, pageable: Pageable?): Page<AcademicProgram> {

        if (!facultyRepository.existsById(idFaculty)) {
            throw BusinessException("The Faculty does not exist!", HttpStatus.NOT_FOUND)
        }

        return academicProgramRepository.findAllByFacultyId(idFaculty, pageable)
    }

    fun activate(idProgram: Long) {

        val academicProgramResult = academicProgramRepository.findById(idProgram)
            .orElseThrow { throw EntityNotFoundException("Program Not Found") }

        if (academicProgramResult.enabled == true) throw BusinessException(
            "This program is already active",
            HttpStatus.PRECONDITION_FAILED
        )

        academicProgramResult.enabled = true

        academicProgramRepository.save(academicProgramResult)
    }

    fun desactive(idProgram: Long) {

        val academicProgramResult = academicProgramRepository.findById(idProgram)
            .orElseThrow { throw EntityNotFoundException("Program Not Found") }

        if (academicProgramResult.enabled == false) throw BusinessException(
            "This program is already desactive",
            HttpStatus.PRECONDITION_FAILED
        )

        academicProgramResult.enabled = false

        academicProgramRepository.save(academicProgramResult)
    }

    fun editProgram(academicProgram: AcademicProgram, idProgram: Long) {

        if (academicProgramRepository.existsById(idProgram)) {

            val academicProgramFound = academicProgramRepository.findById(academicProgram.id)

            if (academicProgramFound.get().id != idProgram) throw BusinessException(
                "Program already created with that ID",
                HttpStatus.PRECONDITION_FAILED
            )
            if (academicProgramRepository.existsByName(academicProgram.name))throw BusinessException("This name already exist!", HttpStatus.PRECONDITION_FAILED)

            academicProgramRepository.save(academicProgram)
        } else {
            throw BusinessException("This Program not found", HttpStatus.NOT_FOUND)
        }
    }
}
