package edu.eam.ingesoft.academic.services

import edu.eam.ingesoft.academic.exceptions.BusinessException
import edu.eam.ingesoft.academic.model.entities.AcademicSpace
import edu.eam.ingesoft.academic.repositories.AcademicProgramRepository
import edu.eam.ingesoft.academic.repositories.AcademicSpaceRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException

@Service
public class AcademicSpaceService {

    @Autowired
    lateinit var academicSpaceRepository: AcademicSpaceRepository

    @Autowired
    lateinit var academicProgramRepository: AcademicProgramRepository

    fun create(academicSpace: AcademicSpace) {
        val academicProgramById = academicProgramRepository.findById(academicSpace.academicProgram.id)
        val academicSpaceByName = academicSpaceRepository.findByName(academicSpace.name)

        if (academicProgramById.isEmpty()) {
            throw BusinessException("The Program does not exist!", HttpStatus.NOT_FOUND)
        }

        if (!academicSpaceByName.isEmpty()) {
            throw BusinessException("Name in use", HttpStatus.PRECONDITION_FAILED)
        }
        academicSpaceRepository.save(academicSpace)
    }

    fun find(idAcademicSpace: Long): AcademicSpace = academicSpaceRepository.findById(idAcademicSpace)
        .orElseThrow { EntityNotFoundException("Academic Space not found.") }

    fun edit(academicSpace: AcademicSpace, idAcademicSpace: Long) {

        val academicSpaceToFind = academicSpaceRepository.findById(idAcademicSpace)

        if (academicSpaceToFind.isEmpty) throw BusinessException("Academic Space doesn´t exist.", HttpStatus.NOT_FOUND)

        val academicProgramById = academicProgramRepository.findById(academicSpace.academicProgram.id)
        val academicSpaceByName = academicSpaceRepository.findByName(academicSpace.name)

        if (academicProgramById.isEmpty()) {
            throw BusinessException("The Program does not exist!", HttpStatus.NOT_FOUND)
        }

        if ((!academicSpaceByName.isEmpty() || academicSpaceByName.size == 1) && academicSpaceByName[0].id != academicSpaceToFind.get().id) {
            throw BusinessException("Name in use", HttpStatus.PRECONDITION_FAILED)
        }

        academicSpaceRepository.save(academicSpace)
    }

    fun getAcademicSpaceListByProgram(idProgram: Long, pageable: Pageable): Page<AcademicSpace> {

        if (!academicProgramRepository.existsById(idProgram)) {
            throw BusinessException("The Program does not exist!", HttpStatus.NOT_FOUND)
        }

        val academicSpaceList = academicSpaceRepository.findAllByProgramId(idProgram, pageable)
        return academicSpaceList
    }

    fun getAcademicSpaceListBySemester(idProgram: Long, semester: Int?, pageable: Pageable): Page<AcademicSpace> {
        if (semester == null) {
            return getAcademicSpaceListByProgram(idProgram, pageable)
        }

        return academicSpaceRepository.findAllBySemester(idProgram, semester, pageable)
    }

    fun activate(id: Long) {
        val academicSpaceToFind = academicSpaceRepository.findById(id).orElse(null) ?: throw
        BusinessException("Academic Space doesn´t exist.", HttpStatus.NOT_FOUND)
        if (academicSpaceToFind.enabled == true) throw BusinessException(
            "Academic Space already activated.",
            HttpStatus.PRECONDITION_FAILED
        )
        academicSpaceToFind.enabled = true
        academicSpaceRepository.save(academicSpaceToFind)
    }

    fun deactivate(id: Long) {

        val academicSpaceToFind = academicSpaceRepository.findById(id).orElse(null) ?: throw
        BusinessException("Academic Space doesn´t exist.", HttpStatus.NOT_FOUND)

        if (academicSpaceToFind.enabled == false) throw BusinessException(
            "Academic Space already deactivated.",
            HttpStatus.PRECONDITION_FAILED
        )
        academicSpaceToFind.enabled = false

        academicSpaceRepository.save(academicSpaceToFind)
    }

    fun findAcademicSpacesByIds(academicIds: List<Long>): MutableIterable<AcademicSpace> =
        academicSpaceRepository.findAllById(academicIds)
}
