package edu.eam.ingesoft.academic.services

import edu.eam.ingesoft.academic.repositories.AcademicPeriodRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AcademicPeriodService {

    @Autowired
    lateinit var academicPeriodRepository: AcademicPeriodRepository

    fun findCurrentAcademicPeriod() = academicPeriodRepository.findCurrentAcademicPeriod()
}
