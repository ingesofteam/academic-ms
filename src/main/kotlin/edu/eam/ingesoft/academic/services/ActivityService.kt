package edu.eam.ingesoft.academic.services

import edu.eam.ingesoft.academic.model.entities.Activity
import edu.eam.ingesoft.academic.repositories.ActivityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException

@Service
class ActivityService {

    @Autowired
    lateinit var activitiRepository: ActivityRepository

    fun find(id: Long): Activity = activitiRepository.findById(id).orElseThrow { EntityNotFoundException("Activity Not Found") }
}
