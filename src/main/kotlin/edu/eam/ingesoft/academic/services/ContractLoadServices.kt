package edu.eam.ingesoft.academic.services

import edu.eam.ingesoft.academic.exceptions.BusinessException
import edu.eam.ingesoft.academic.model.entities.ContractLoad
import edu.eam.ingesoft.academic.repositories.ContractLoadRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException

@Service
class ContractLoadServices {

    @Autowired
    lateinit var contractLoadRepository: ContractLoadRepository

    fun create(contractLoad: ContractLoad) {
        if (contractLoadRepository.existsById(contractLoad.id)) throw BusinessException("Contract already exists", HttpStatus.PRECONDITION_FAILED)

        if (contractLoadRepository.existsByName(contractLoad.name)) throw BusinessException("Name already exists", HttpStatus.PRECONDITION_FAILED)

        contractLoadRepository.save(contractLoad)
    }

    fun edit(contractLoad: ContractLoad, id: Long) {
        if (!contractLoadRepository.existsById(id)) throw BusinessException("Contract don't exist", HttpStatus.NOT_FOUND)

        val contractLoadName = contractLoadRepository.findByName(contractLoad.name)

        if (contractLoadName.isNotEmpty()) throw BusinessException("Name is already in use", HttpStatus.PRECONDITION_FAILED)

        contractLoadRepository.save(contractLoad)
    }

    fun findContractLoad() = contractLoadRepository.findAll()

    fun deactivate(id: Long) {
        val contractLoad = contractLoadRepository.findById(id).orElseThrow { EntityNotFoundException("Contract not found") }

        if (contractLoad.enabled == false) throw BusinessException("Contract already deactivated", HttpStatus.PRECONDITION_FAILED)

        contractLoad.enabled = false

        contractLoadRepository.save(contractLoad)
    }

    fun activate(id: Long) {
        val contractLoad = contractLoadRepository.findById(id).orElseThrow { EntityNotFoundException("Contract not found") }

        if (contractLoad.enabled == true) throw BusinessException("Contract already activated", HttpStatus.PRECONDITION_FAILED)

        contractLoad.enabled = true

        contractLoadRepository.save(contractLoad)
    }
}
