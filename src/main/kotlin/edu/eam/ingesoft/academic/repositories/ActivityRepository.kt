package edu.eam.ingesoft.academic.repositories

import edu.eam.ingesoft.academic.model.entities.Activity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ActivityRepository : CrudRepository<Activity, Long>
