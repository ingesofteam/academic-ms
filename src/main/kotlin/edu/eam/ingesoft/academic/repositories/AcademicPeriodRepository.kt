package edu.eam.ingesoft.academic.repositories

import edu.eam.ingesoft.academic.model.entities.AcademicPeriod
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface AcademicPeriodRepository : CrudRepository<AcademicPeriod, String> {

    @Query("SELECT ap FROM AcademicPeriod ap WHERE CURRENT_DATE BETWEEN ap.startDate AND ap.endDate")
    fun findCurrentAcademicPeriod(): AcademicPeriod
}
