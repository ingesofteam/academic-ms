package edu.eam.ingesoft.academic.controllers

import edu.eam.ingesoft.academic.config.Groups
import edu.eam.ingesoft.academic.config.Permissions
import edu.eam.ingesoft.academic.config.Routes
import edu.eam.ingesoft.academic.model.entities.AcademicSpace
import edu.eam.ingesoft.academic.model.request.FindAcademicSpacesIdRequest
import edu.eam.ingesoft.academic.security.Secured
import edu.eam.ingesoft.academic.services.AcademicSpaceService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.ACADEMIC_SPACES_PATH)
class AcademicSpaceController {

    @Autowired
    lateinit var academicSpaceService: AcademicSpaceService

    @Secured(
        permissions = [Permissions.CREATE_ACADEMIC_SPACE],
        groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @PostMapping
    fun create(@RequestBody academicSpace: AcademicSpace) = academicSpaceService.create(academicSpace)

    @Secured(
        permissions = [Permissions.EDIT_ACADEMIC_SPACE],
        groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @PutMapping(Routes.EDIT_ACADEMIC_SPACES_PATH)
    fun edit(@RequestBody academicSpace: AcademicSpace, @PathVariable("idAcademicSpace") idAcademicSpace: Long) =
        academicSpaceService.edit(academicSpace, idAcademicSpace)

    @Secured(
        permissions = [Permissions.FIND_ACADEMIC_SPACE],
        groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @GetMapping(Routes.FIND_ACADEMIC_SPACES_PATH)
    fun find(@PathVariable("idAcademicSpace") idAcademicSpace: Long) = academicSpaceService.find(idAcademicSpace)

    @Secured(
        permissions = [Permissions.ACTIVATE_ACADEMIC_SPACE],
        groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @PatchMapping(Routes.ACTIVATE_ACADEMIC_SPACE)
    fun activate(@PathVariable("id") id: Long) = academicSpaceService.activate(id)

    @Secured(
        permissions = [Permissions.DESACTIVE_ACADEMIC_SPACE],
        groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @PatchMapping(Routes.DEACTIVATE_ACADEMIC_SPACE)
    fun deactivate(@PathVariable("id") id: Long) = academicSpaceService.deactivate(id)

    @PostMapping(Routes.FIND_ACADEMIC_SPACES_BY_IDS_PATH)
    fun findAcademicSpacesByIds(@RequestBody findAcademicSpacesIdRequest: FindAcademicSpacesIdRequest) =
        academicSpaceService.findAcademicSpacesByIds(findAcademicSpacesIdRequest.ids)
}
