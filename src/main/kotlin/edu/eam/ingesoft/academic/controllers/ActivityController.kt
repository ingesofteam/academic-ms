package edu.eam.ingesoft.academic.controllers

import edu.eam.ingesoft.academic.config.Routes
import edu.eam.ingesoft.academic.services.ActivityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.ACTIVITY_PATH)
class ActivityController {

    @Autowired
    lateinit var activityService: ActivityService

    @GetMapping(Routes.FIND_ACTIVITY_PATH)
    fun find(@PathVariable("idActivity") idActivity: Long) = activityService.find(idActivity)
}
