package edu.eam.ingesoft.academic.security.authclient

import edu.eam.ingesoft.academic.security.SecurityException
import edu.eam.ingesoft.academic.security.authclient.model.SecurityPayload
import edu.eam.ingesoft.academic.security.authclient.model.Token
import feign.FeignException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.lang.Exception

@Component
class SecurityService {

    @Autowired
    lateinit var securityClient: SecurityClient

    fun validateToken(token: String): SecurityPayload {
        try {
            return securityClient.validateToken(Token(token))
        } catch (exc: FeignException.Forbidden) {
            exc.printStackTrace()
            throw SecurityException("Invalid token")
        } catch (exc: FeignException) {
            exc.printStackTrace()
            throw Exception("Unexpected Exception")
        }
    }
}
