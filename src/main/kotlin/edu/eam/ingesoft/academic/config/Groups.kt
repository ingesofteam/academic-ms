package edu.eam.ingesoft.academic.config

object Groups {
    const val PROGRAM_DIRECTOR = "programDirector"
    const val SYSTEM_ADMINISTRATOR = "systemAdministrator"
}
