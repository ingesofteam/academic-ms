package edu.eam.ingesoft.academic.exceptions

import org.springframework.http.HttpStatus

class BusinessException(message: String?, val status: HttpStatus = HttpStatus.PRECONDITION_FAILED) :
    RuntimeException(message)
