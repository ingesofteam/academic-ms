package edu.eam.ingesoft.academic.model.request

data class EditFacultyRequest(
    val id: Long,
    val name: String,
    val status: Boolean
)
