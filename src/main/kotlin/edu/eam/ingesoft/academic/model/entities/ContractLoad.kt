package edu.eam.ingesoft.academic.model.entities

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "contract_loads")
data class ContractLoad(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,

    val name: String,

    @Column(name = "weekly_hours_qty")
    val weeklyHoursQty: Int,

    var enabled: Boolean? = true
)
