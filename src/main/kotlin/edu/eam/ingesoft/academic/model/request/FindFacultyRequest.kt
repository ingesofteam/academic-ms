package edu.eam.ingesoft.academic.model.request

data class FindFacultyRequest(
    val id: Long,
    val name: String,
    val status: Boolean
)
