package edu.eam.ingesoft.academic.model.request

data class ListallFacultyRequest(
    val id: Long,
    val name: String,
    val status: Boolean
)
