package edu.eam.ingesoft.academic.model.entities

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "academic_programs")
data class AcademicProgram(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,

    val name: String,

    val snies: String,

    val credits: Int,

    @ManyToOne
    @JoinColumn(name = "id_faculty", referencedColumnName = "id")
    val faculty: Faculty,

    @Column(name = "semester_qty")
    val semesterQty: Int,

    var enabled: Boolean? = false
)
