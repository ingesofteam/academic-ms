package edu.eam.ingesoft.academic.model.request

data class FindAcademicSpacesIdRequest(
    val ids: List<Long>
)
