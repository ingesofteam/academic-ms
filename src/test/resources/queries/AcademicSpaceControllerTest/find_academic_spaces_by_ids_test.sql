INSERT INTO faculties(id, "name", status) VALUES(100, 'Faculty 1', false);


INSERT INTO academic_programs (id, credits, enabled, "name", semester_qty, snies, id_faculty) VALUES(100, 85, true, 'Software', 0, '101010', 100);

INSERT INTO academic_spaces(id, credits, enabled, max_faults, modality, "name", id_academic_program, semester)VALUES(100, 10,false, 0, '13', 'Space 1', 100, 1);
INSERT INTO academic_spaces(id, credits, enabled, max_faults, modality, "name", id_academic_program, semester)VALUES(101, 10,false, 0, '13', 'Space 2', 100, 1);
INSERT INTO academic_spaces(id, credits, enabled, max_faults, modality, "name", id_academic_program, semester)VALUES(102, 10,false, 0, '13', 'Space 3', 100, 1);
INSERT INTO academic_spaces(id, credits, enabled, max_faults, modality, "name", id_academic_program, semester)VALUES(103, 10,false, 0, '13', 'Space 4', 100, 1);

