package edu.eam.ingesoft.academic.controllers

import CommonTests
import edu.eam.ingesoft.academic.config.Groups
import edu.eam.ingesoft.academic.config.Permissions
import edu.eam.ingesoft.academic.config.Routes
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.time.LocalDate

class AcademicPeriodControllerTest : CommonTests() {

    @Test
    @Sql("/queries/academicPeriodControllerTest/getCurrentPeriodTest.sql")
    fun getCurrentPeriodTest() {

        val today = LocalDate.now()

        val period = if (today.month.value > 6) "2" else "1"

        val header = mockSecurity("", listOf(Permissions.CHECK_CURRENT_ACADEMIC_PERIOD), listOf(Groups.SYSTEM_ADMINISTRATOR))

        val request = MockMvcRequestBuilders.get("${Routes.ACADEMICPERIOD_PATH}/${Routes.CURRENT_PERIOD_PTH}")
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.id",
                    Matchers.`is`("${today.year}$period")
                )
            )
    }

    @Test
    fun getCurrentPeriodTestNotFound() {
        val header = mockSecurity("", listOf(Permissions.CHECK_CURRENT_ACADEMIC_PERIOD), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders.get("${Routes.ACADEMICPERIOD_PATH}/${Routes.CURRENT_PERIOD_PTH}")
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
    }
}
