package edu.eam.ingesoft.academic.controllers

import CommonTests
import edu.eam.ingesoft.academic.config.Groups
import edu.eam.ingesoft.academic.config.Permissions
import edu.eam.ingesoft.academic.config.Routes
import edu.eam.ingesoft.academic.model.entities.AcademicProgram
import edu.eam.ingesoft.academic.model.entities.Faculty
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class AcademicProgramControllerTest : CommonTests() {

    @Test
    @Sql("/queries/academicProgramControllerTest/createAcademicProgram.sql")
    fun createAcademicProgramTest() {

        val faculty = facultyRepository.findById(1).orElse(null)
        val academicProgram = AcademicProgram(1, "Negocios", "0001", 4, faculty, 2, true)

        val header = mockSecurity("Token", listOf(Permissions.CREATE_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders.post(Routes.ACADEMICPROGRAMS_PATH)
            .content(objectMapper.writeValueAsString(academicProgram)).contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val academicPrograms = academicProgramRepository.findAll().asSequence().toList()

        val academicProgramtoAssert = academicPrograms.find { it.name == academicProgram.name }

        Assert.assertNotNull(academicProgramtoAssert)
        Assert.assertEquals(academicProgram.name, academicProgram.name)
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/createAcademicPropestyRepitName.sql")
    fun createAcademicProgramWithRepetiNameTest() {

        val faculty = facultyRepository.findById(1).orElse(null)

        val academicProgram = AcademicProgram(2, "Negocios", "0001", 4, faculty, 2, true)

        val header = mockSecurity("Token", listOf(Permissions.CREATE_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders.post(Routes.ACADEMICPROGRAMS_PATH)
            .content(objectMapper.writeValueAsString(academicProgram))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("This program is already registered in the system: ")
                )
            )
    }

    @Test
    fun createAcademicProgramNotFoundFacultyTest() {

        val faculty = Faculty(2, "Manuel", true)

        val academicProgram = AcademicProgram(1, "Negocios", "0001", 4, faculty, 2, true)

        val header = mockSecurity("Token", listOf(Permissions.CREATE_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders.post(Routes.ACADEMICPROGRAMS_PATH)
            .content(objectMapper.writeValueAsString(academicProgram))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The Faculty does not exist!")))
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/findAcademicProgram.sql")
    fun findAcademicProgramTest() {

        val idProgram: Long = 1L

        val header = mockSecurity("Token", listOf(Permissions.FIND_ACADEMIC_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request =
            MockMvcRequestBuilders.get("${Routes.ACADEMICPROGRAMS_PATH}/${Routes.FIND_PROGRAM_PATH}", idProgram)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.`is`(1)))
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/findAcademicProgram.sql")
    fun findAcademicProgramNotFoundTest() {

        val id: Long = 3L

        val header = mockSecurity("Token", listOf(Permissions.FIND_ACADEMIC_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders.get("${Routes.ACADEMICPROGRAMS_PATH}/${Routes.FIND_PROGRAM_PATH}", id)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The program does not exist")))
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/getProgramsFromFaculty.sql")
    fun getAllProgramFromFacultyTest() {

        val idFaculty: Long = 2L

        val header = mockSecurity("Token", listOf(Permissions.LIST_PROGRAMS_BY_FACULTY), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders.get("${Routes.ACADEMICPROGRAMS_PATH}/${Routes.GET_ALL_PROGRAMS_BY_FACULTY_ID}?size=5&sort=id", idFaculty)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(5)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[0].faculty.id", Matchers.`is`(2)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[1].faculty.id", Matchers.`is`(2)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[2].faculty.id", Matchers.`is`(2)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[3].faculty.id", Matchers.`is`(2)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[4].faculty.id", Matchers.`is`(2)))
            .andExpect(MockMvcResultMatchers.jsonPath("pageable.page_size", Matchers.`is`(5)))
            .andExpect(MockMvcResultMatchers.jsonPath("pageable.sort.sorted", Matchers.`is`(true)))
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/getProgramsFromFaculty.sql")
    fun getAllProgramFromFacultyNotFoundTest() {

        val idFaculty: Long = 1L

        val header = mockSecurity("Token", listOf(Permissions.LIST_PROGRAMS_BY_FACULTY), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders.get(
            "${Routes.ACADEMICPROGRAMS_PATH}/${Routes.GET_ALL_PROGRAMS_BY_FACULTY_ID}",
            idFaculty
        ).contentType(MediaType.APPLICATION_JSON_VALUE).header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The Faculty does not exist!")))
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/changeStatusProgamNotFound.sql")
    fun activeProgramTest() {

        val idProgram: Long = 1L

        val header = mockSecurity("Token", listOf(Permissions.ACTIVE_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders.patch(
            "${Routes.ACADEMICPROGRAMS_PATH}/${Routes.CHANGE_STATUS_PROGRAM_ACTIVE}",
            idProgram
        ).contentType(MediaType.APPLICATION_JSON).header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val academicProgram = academicProgramRepository.findById(idProgram)

        Assert.assertNotNull(academicProgram)
        Assert.assertEquals(true, academicProgram.get().enabled)
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/changeStatusProgram.sql")
    fun activeProgramNotFoundTest() {

        val idProgram: Long = 2L

        val header = mockSecurity("Token", listOf(Permissions.ACTIVE_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders.patch(
            "${Routes.ACADEMICPROGRAMS_PATH}/${Routes.CHANGE_STATUS_PROGRAM_ACTIVE}",
            idProgram
        ).contentType(MediaType.APPLICATION_JSON).header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Program Not Found")))
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/changeStatusProgramAlreadyActive.sql")
    fun activeProgramAlreadyActive() {

        val idProgram: Long = 1L

        val header = mockSecurity("Token", listOf(Permissions.ACTIVE_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders.patch(
            "${Routes.ACADEMICPROGRAMS_PATH}/${Routes.CHANGE_STATUS_PROGRAM_ACTIVE}",
            idProgram
        ).contentType(MediaType.APPLICATION_JSON).header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("This program is already active")))
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/changeStatusProgramDesactive.sql")
    fun desactiveProgramTest() {

        val idProgram: Long = 1L

        val header = mockSecurity("Token", listOf(Permissions.DESACTIVE_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders.patch(
            "${Routes.ACADEMICPROGRAMS_PATH}/${Routes.CHANGE_STATUS_PROGRAM_DESACTIVE}",
            idProgram
        ).contentType(MediaType.APPLICATION_JSON).header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val academicProgram = academicProgramRepository.findById(idProgram)

        Assert.assertNotNull(academicProgram)
        Assert.assertEquals(false, academicProgram.get().enabled)
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/changeStatusProgramAlreadyDesactive.sql")
    fun desactiveProgramNOtFound() {

        val idProgram: Long = 1L
        val header = mockSecurity("Token", listOf(Permissions.DESACTIVE_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders.patch(
            "${Routes.ACADEMICPROGRAMS_PATH}/${Routes.CHANGE_STATUS_PROGRAM_DESACTIVE}",
            idProgram
        ).contentType(MediaType.APPLICATION_JSON).header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("This program is already desactive")))
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/changeStatusProgramDesactiveNotFound.sql")
    fun desactiveProgramNotFoundTest() {

        val idProgram: Long = 2L
        val header = mockSecurity("Token", listOf(Permissions.DESACTIVE_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders.patch(
            "${Routes.ACADEMICPROGRAMS_PATH}/${Routes.CHANGE_STATUS_PROGRAM_DESACTIVE}",
            idProgram
        ).contentType(MediaType.APPLICATION_JSON).header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Program Not Found")))
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/editProgram.sql")
    fun editProgram() {
        val idProgram: Long = 1L
        val faculty = facultyRepository.findById(1).orElse(null)
        val academicProgram = AcademicProgram(1, "Negocios", "0001", 4, faculty, 2, true)

        val header = mockSecurity("Token", listOf(Permissions.EDIT_ACADEMIC_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders.put("${Routes.ACADEMICPROGRAMS_PATH}/${Routes.EDIT_PROGRAM}", idProgram)
            .content(objectMapper.writeValueAsString(academicProgram))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val academicPrograms = academicProgramRepository.findAll().asSequence().toList()

        val academicProgramToAssert = academicPrograms.find { it.id == academicProgram.id }

        Assert.assertNotNull(academicProgramToAssert)
        Assert.assertEquals(academicProgram.name, academicProgramToAssert?.name)
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/editProgramNotFound.sql")
    fun editProgramNotFound() {
        val idProgram: Long = 3L
        val faculty = facultyRepository.findById(1).orElse(null)
        val academicProgram = AcademicProgram(1, "Negocios", "0001", 4, faculty, 2, true)

        val header = mockSecurity("Token", listOf(Permissions.EDIT_ACADEMIC_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders.put("${Routes.ACADEMICPROGRAMS_PATH}/${Routes.EDIT_PROGRAM}", idProgram)
            .content(objectMapper.writeValueAsString(academicProgram))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound).andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/editProgramRepitName.sql")
    fun editProgramRepitName() {
        val idProgram: Long = 1L
        val faculty = facultyRepository.findById(1).orElse(null)
        val academicProgram = AcademicProgram(1, "Negocios", "0001", 4, faculty, 2, true)

        val header = mockSecurity("Token", listOf(Permissions.EDIT_ACADEMIC_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders.put("${Routes.ACADEMICPROGRAMS_PATH}/${Routes.EDIT_PROGRAM}", idProgram)
            .content(objectMapper.writeValueAsString(academicProgram))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed).andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
    }

    @Test
    @Sql("/queries/academicProgramControllerTest/editProgramAlreadyExisteId.sql")
    fun editProgramAlreadyExistId() {

        val idProgram: Long = 1L
        val faculty = facultyRepository.findById(1).orElse(null)
        val academicProgram = AcademicProgram(2, "Matematicas", "0001", 4, faculty, 2, true)

        val header = mockSecurity("Token", listOf(Permissions.EDIT_ACADEMIC_PROGRAM), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders.put("${Routes.ACADEMICPROGRAMS_PATH}/${Routes.EDIT_PROGRAM}", idProgram)
            .content(objectMapper.writeValueAsString(academicProgram))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed).andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
    }
}
