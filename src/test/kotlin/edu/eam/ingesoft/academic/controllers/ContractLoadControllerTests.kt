package edu.eam.ingesoft.academic.controllers

import CommonTests
import edu.eam.ingesoft.academic.config.Groups
import edu.eam.ingesoft.academic.config.Permissions
import edu.eam.ingesoft.academic.config.Routes
import edu.eam.ingesoft.academic.model.entities.ContractLoad
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class ContractLoadControllerTests : CommonTests() {

    @Test
    fun createContractLoadTest() {
        val contract = ContractLoad(1L, "contrato1", 5, true)

        val header = mockSecurity("Token", listOf(Permissions.CREATE_CONTRACT_LOAD), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .post(Routes.CONTRACT_PATH)
            .content(objectMapper.writeValueAsString(contract))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val contracts = contractLoadRepository.findAll().asSequence().toList()
        val contractToAssert = contracts.find { it.name == contract.name }

        Assert.assertNotNull(contractToAssert)
        Assert.assertEquals(contract.name, contractToAssert?.name)
    }

    @Test
    @Sql("/queries/contractloadcontrollertest/create_contract_load_already_exists_test.sql")
    fun createContractLoadAlreadyExistsTest() {
        val contract = ContractLoad(1L, "contrato1", 5, true)

        val header = mockSecurity("Token", listOf(Permissions.CREATE_CONTRACT_LOAD), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .post(Routes.CONTRACT_PATH)
            .content(objectMapper.writeValueAsString(contract))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Contract already exists")))
    }

    @Test
    @Sql("/queries/contractloadcontrollertest/create_contract_load_name_already_exists_test.sql")
    fun createContractLoadNameAlreadyExistsTest() {
        val contract = ContractLoad(2L, "contrato1", 5, true)

        val header = mockSecurity("Token", listOf(Permissions.CREATE_CONTRACT_LOAD), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .post(Routes.CONTRACT_PATH)
            .content(objectMapper.writeValueAsString(contract))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Name already exists")))
    }

    @Test
    @Sql("/queries/contractloadcontrollertest/edit_contract_load_test.sql")
    fun editContractLoadTest() {
        val contract = ContractLoad(1, "contrato2", 5, true)

        val header = mockSecurity("Token", listOf(Permissions.EDIT_CONTRACT_TYPE), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .put(Routes.CONTRACT_PATH + "/1")
            .content(objectMapper.writeValueAsString(contract))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val contracts = contractLoadRepository.findAll().asSequence().toList()

        val contractToAssert = contracts.find { it.name == contract.name }

        Assert.assertNotNull(contractToAssert)
        Assert.assertEquals(contract.name, contractToAssert?.name)
    }

    @Test
    @Sql("/queries/contractloadcontrollertest/edit_contract_load_not_exist_test.sql")
    fun editContractLoadNotExistTest() {
        val contract = ContractLoad(1L, "contrato2", 5, true)

        val header = mockSecurity("Token", listOf(Permissions.EDIT_CONTRACT_TYPE), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .put(Routes.CONTRACT_PATH + "/2")
            .content(objectMapper.writeValueAsString(contract))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Contract don't exist")))
    }

    @Test
    @Sql("/queries/contractloadcontrollertest/edit_contract_load_with_repeated_name_test.sql")
    fun editContractLoadWithRepeatedNameTest() {
        val contract = ContractLoad(1L, "contrato2", 5, true)

        val header = mockSecurity("Token", listOf(Permissions.EDIT_CONTRACT_TYPE), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .put(Routes.CONTRACT_PATH + "/1")
            .content(objectMapper.writeValueAsString(contract))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Name is already in use")))
    }

    @Test
    @Sql("/queries/contractloadcontrollertest/list_contract_load_test.sql")
    fun listContractLoadTest() {
        val request = MockMvcRequestBuilders
            .get("${Routes.CONTRACT_PATH}")
            .contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize<Int>(2)))
    }

    @Test
    fun listContractLoadEmptyTest() {
        val request = MockMvcRequestBuilders
            .get("${Routes.CONTRACT_PATH}")
            .contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize<Int>(0)))
    }

    @Test
    @Sql("/queries/contractloadcontrollertest/deactivate_contract_test.sql")
    fun deactivateContractTest() {
        val header = mockSecurity("Token", listOf(Permissions.DEACTIVATE_CONTRACT_TYPE), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .patch(Routes.CONTRACT_PATH + "/1/deactivate")
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val contracts = contractLoadRepository.findAll().asSequence().toList()

        val contractToAssert = contracts.find { it.id == 1L }

        Assert.assertNotNull(contractToAssert)
        Assert.assertEquals(false, contractToAssert?.enabled)
    }

    @Test
    fun deactivateContractNotExistTest() {
        val header = mockSecurity("Token", listOf(Permissions.DEACTIVATE_CONTRACT_TYPE), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .patch(Routes.CONTRACT_PATH + "/11/deactivate")
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Contract not found")))
    }

    @Test
    @Sql("/queries/contractloadcontrollertest/contract_already_deactivate_test.sql")
    fun contractAlreadyDeactivateTest() {
        val header = mockSecurity("Token", listOf(Permissions.DEACTIVATE_CONTRACT_TYPE), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .patch(Routes.CONTRACT_PATH + "/1/deactivate")
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Contract already deactivated")))
    }

    @Test
    @Sql("/queries/contractloadcontrollertest/activate_contract_test.sql")
    fun activateContractTest() {
        val header = mockSecurity("Token", listOf(Permissions.ACTIVATE_CONTRACT_TYPE), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .patch(Routes.CONTRACT_PATH + "/1/activate")
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val contracts = contractLoadRepository.findAll().asSequence().toList()
        val contractToAssert = contracts.find { it.id == 1L }

        Assert.assertNotNull(contractToAssert)
        Assert.assertEquals(true, contractToAssert?.enabled)
    }

    @Test
    fun activateContractNotExistTest() {
        val header = mockSecurity("Token", listOf(Permissions.ACTIVATE_CONTRACT_TYPE), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .patch(Routes.CONTRACT_PATH + "/11/activate")
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Contract not found")))
    }

    @Test
    @Sql("/queries/contractloadcontrollertest/contract_already_activate_test.sql")
    fun contractAlreadyActivateTest() {
        val header = mockSecurity("Token", listOf(Permissions.ACTIVATE_CONTRACT_TYPE), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .patch(Routes.CONTRACT_PATH + "/1/activate")
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Contract already activated")))
    }
}
